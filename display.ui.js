/**
 * @file Module js pour gérer l'affichage / masquage de question / réponse / page
 * D'après l'ancien url.js de l'outil labomep
 */

import forwardImage from './public/images/forward.png'

/* eslint-disable camelcase */
import dom from 'sesajstools/dom'
import log from 'sesajstools/utils/log'

import page from 'client/page'

const { addElement, getElement } = dom
// attention à mettre la même chose que dans display et le css
const divIframeId = 'page'

const highLight = ($jqElt) => $jqElt.css('background-color', '#e9e9e9')
const unLight = ($jqElt) => $jqElt.css('background-color', '#fff')

const wikiFormat = (str) => str
  // les tags html autorisés
  .replace(/<br *\/?>/g, '\n') // <br> => \n
  .replace(/<(\/?)(b|em|i|strong|sub|sup|u)>/g, '§§#§§$1$2##§##') // <sub>
  // on vire le reste
  .replace(/</g, '&lt;')
  .replace(/>/g, '&gt;')
  // on remet ceux qu'on a mis de coté
  .replace(/§§#§§/g, '<')
  .replace(/##§##/g, '>')
  // et on gère notre syntaxe wiki maison
  .replace(/\n/g, '<br>\n')
  .replace(/\*\*(.*?)\*\*/g, '<strong>$1</strong>')
  .replace(/\/\/(.*?)\/\//g, '<em>$1</em>')
  .replace(/__(.*?)__/g, '<u>$1</u>')
  .replace(/\^([^ ]+)/g, '<sup>$1</sup>')

/**
 * Retourne une seule fonction qui affectera les comportements de l'interface
 * avec la gestion des étapes pour les ressources url
 * @service plugins/url/displayUi
 * @param {Ressource}      ressource
 * @param {displayOptions} options
 * @param {errorCallback}  next
 */
async function displayUi (ressource, options, next) {
  import('jquery-ui/themes/base/core.css')
  import('jquery-ui/themes/base/theme.css')
  import('jquery-ui/themes/base/dialog.css')
  const { default: $ } = await import('jquery')
  await import('jquery-ui/ui/widgets/dialog')
  // ici on avait un pb, $.ui.dialog n'existait pas !
  // c'est lié au fait que le jQuery récupéré par dialog.js n'était pas la même instance
  // que la notre (car on a du node_modules/jquery et du app/plugins/node_modules/jquery)
  // Pour régler ça, il faudrait un seul node_modules à la racine, mais en attendant on
  // contourne le pb avec un alias webpack (dans sesatheque) pour que le `require('jquery')`
  // prenne toujours celui de la racine

  // faut attendre que jQuery soit ready
  $(async function onReady () {
    function addReponseDialog () {
      const form = addElement(divReponse, 'form', { action: '' })
      const textarea = addElement(form, 'textarea', { id: 'answer', cols: '50', rows: '10' })

      if (sendResultat) {
        // qqun veut récupérer la réponse
        addElement(form, 'br')
        const boutonReponse = addElement(form, 'button', { id: 'envoi' }, 'Enregistrer cette réponse')
        // on ajoute l'envoi de la réponse sur le bouton et à la fermeture
        boutonReponse.addEventListener('click', (event) => {
          event.preventDefault() // faut pas poster le form sinon ça recharge la page/iframe
          if (isResultatSent) return log('Résultat déjà envoyé')
          sendResultat(textarea.value, false, function (retour) {
            if (retour && (retour.ok || retour.success)) isResultatSent = true
          })
        })
        document.body.addEventListener('unload', () => sendResultat(null, true))
        textarea.addEventListener('change', () => { isResultatSent = false })
      } else if (options.preview) {
        addElement(form, 'p', null, "Réponse attendue mais pas d'envoi possible en prévisualisation")
      } else {
        addElement(form, 'p', { class: 'info', style: { margin: '1em;' } },
          "Aucun enregistrement ne sera effectué (car aucune destination n'a été fournie pour l'envoyer, c'est normal en visualisation seule)")
      }
    } // addReponseDialog

    /**
     * Retourne un DomElement img créé pour l'occasion
     * @return {HTMLElement}
     */
    function getLienSuivant () {
      const lien = getElement('img', {
        class: 'lienSuivant',
        src: forwardImage,
        align: 'absmiddle',
        alt: 'suivant'
      })
      if (etapes.hasNext()) $(lien).click(etapes.next)
      return lien
    }

    /**
     * Initialise les fenêtres modales
     */
    function init () {
      // les comportements qui dépendent pas du contexte
      $lienConsigne.click(consigne.toggle)
      $lienReponse.click(reponse.toggle)
      $lienInfo.click(information.toggle)

      // les fenetres modales
      const informationDialogOptions = {
        autoOpen: false,
        // buttons: {'Vu': () => $information.dialog('close')},
        resizable: false,
        // collision none change rien, ça danse quand on enlève un dialog
        // collision fit idem
        position: { my: 'left top+50', at: 'left bottom', of: '#entete' },
        title: 'Information'
      }
      $information.dialog(informationDialogOptions)

      const consigneDialogOptions = {
        autoOpen: false,
        position: { my: 'center top+200', at: 'center bottom', of: '#entete' },
        resizable: true,
        title: 'Consigne',
        width: 450
      }
      $consigne.dialog(consigneDialogOptions)

      const reponseDialogOptions = {
        autoOpen: false,
        position: { collision: 'fit', my: 'right bottom', at: 'right bottom', of: '#page' },
        resizable: true,
        title: 'Ta réponse',
        width: 480
      }
      $reponse.dialog(reponseDialogOptions)
    } // init

    /**
     * Charge les éventuelles dépendances avant d'appeler next
     * @private
     * @param next
     */
    function loadDependencies (next) {
      const dependances = []
      if (hasTexConsigne && typeof MathJax === 'undefined') dependances.push('mathjax')
      if (dependances.length) page.loadAsync(dependances, next)
      else next()
    }

    /**
     * Construit la liste des étapes d'après les options
     * @private
     */
    function setEtapes () {
      let hasInfo = true
      /**
       * Option de l'affichage de la question qui peut prendre les valeurs
       *   'off'    : pas de question
       *   'before' : avant la page
       *   'while'  : pendant la page
       *   'after'  : après la page
       * @type {string}
       */
      const question_option = ressource.parametres.question_option || 'off'
      /**
       * Option de l'affichage de la réponse qui peut prendre les valeurs
       *   'off'      : pas de réponse attendue
       *   'question' : pendant l'affichage de la question
       *   'while'    : pendant l'affichage de la page
       *   'after'    : après la page
       * @type {string}
       */
      const answer_option = ressource.parametres.answer_option || 'off'

      // pas de question
      if (question_option === 'off') {
        if (answer_option === 'while') {
          etapes.liste = [[information, iframe, reponse]]
          etapes.titres = ['Visualisation du document et réponse']
          information.setContent('Observe ce document et envoie ta réponse.')
        } else if (answer_option === 'after') {
          etapes.liste = [[information, iframe], [reponse]]
          etapes.titres = ['Visualisation du document', 'Réponse']
          information.setContent('Observe ce document puis clique sur ', getLienSuivant(), ' pour répondre.')
        } else {
          // off (car pas de before possible pour la réponse)
          etapes.liste = [[iframe]]
          etapes.titres = ['Visualisation du document']
          hasInfo = false
        }

      // question avant
      } else if (question_option === 'before') {
        // consigne puis page
        if (answer_option === 'off') {
          etapes.liste = [[information, consigne], [iframe]]
          etapes.titres = ['Lecture de la consigne', 'Visualisation du document']
          information.setContent('Commence par lire la consigne, puis clique sur ', getLienSuivant(), ' pour voir le document.')
        } else if (answer_option === 'while') {
          etapes.liste = [[information, consigne], [iframe, reponse]]
          etapes.titres = ['Lecture de la consigne', 'Visualisation du document et réponse']
          information.setContent('Lis la consigne, clique sur ', getLienSuivant(), ' pour voir le document et répondre.')
        } else if (answer_option === 'after') {
          etapes.liste = [[information, consigne], [iframe], [reponse]]
          etapes.titres = ['Lecture de la consigne', 'Visualisation du document', 'Réponse']
          information.setContent('Lis la consigne, clique sur ', getLienSuivant(), ' pour voir le document, puis encore une fois pour répondre.')
        } else {
          // answer_option = question
          etapes.liste = [[information, consigne, reponse], [iframe]]
          etapes.titres = ['Lecture de la consigne et réponse', 'Visualisation du document']
          information.setContent('Lis la consigne et donne ta réponse puis clique sur ', getLienSuivant(), ' pour voir le document.')
          // réponse avant l'info
        }

      // question pendant
      } else if (question_option === 'while') {
        if (answer_option === 'after') {
          etapes.liste = [[information, consigne, iframe], [reponse]]
          etapes.titres = ['Réponse', 'Visualisation de la consigne et du document']
          information.setContent('Lis la consigne, observe bien le document puis clique sur ', getLienSuivant(), ' pour pouvoir répondre.')
        } else if (answer_option === 'while' || answer_option === 'question') {
          etapes.liste = [[information, consigne, iframe, reponse]]
          etapes.titres = ['Consigne, visualisation du document et réponse']
          information.setContent('Lis la consigne et observe bien le document avant de répondre.')
        } else {
          // off
          etapes.liste = [[consigne, iframe]]
          etapes.titres = ['Consigne et visualisation du document']
          hasInfo = false
        }

      // question après
      } else if (question_option === 'after') {
        if (answer_option === 'off') {
          etapes.liste = [[information, iframe], [consigne]]
          etapes.titres = ['Visualisation du document', 'consigne']
          information.setContent('Observe bien le document puis clique sur ', getLienSuivant(), ' pour lire la consigne.')
        } else if (answer_option === 'after') {
          etapes.liste = [[information, iframe], [consigne], [reponse]]
          etapes.titres = ['Visualisation du document', 'Lecture de la consigne', 'Réponse']
          information.setContent('Observe bien le document puis clique sur ', getLienSuivant(), ' pour lire la consigne et encore une fois pour répondre.')
        } else {
          // while ou question
          etapes.liste = [[information, iframe], [consigne, reponse]]
          etapes.titres = ['Visualisation du document', 'Consigne et réponse']
          information.setContent('Observe bien le document puis clique sur ', getLienSuivant(), ' pour lire la consigne et répondre.')
        }
      }

      // les rares cas où_l'affichage de l'information n'a pas de sens sont ci-dessus avec hasInfo = false
      if (hasInfo) {
        $lienInfo.show()
        $information.dialog('open')
      } else {
        $lienInfo.hide()
      }
    } // setEtapes

    /**
     * Réactualise l'affichage avec l'étape etapes.currentIndex
     * @private
     */
    function showEtape () {
      const { currentIndex } = etapes
      const etape = etapes.liste[currentIndex]
      const titre = etapes.titres[currentIndex]
      // log(`showEtape ${currentIndex} ${titre}`)
      let i
      // on cache tout
      information.desactiver()
      reponse.desactiver()
      consigne.desactiver()
      iframe.desactiver()

      // active les elts de l'etape en cours
      log(`étape ${etapes.currentIndex}`, etapes)
      log(`étape ${etapes.currentIndex} (${etape.map(e => e.name).join('+')}) ${titre}`)
      etape.forEach(item => item.activer())
      // cache les boutons close
      $('.ui-dialog-titlebar-close').hide()

      // reconstruction du fil d'ariane, titre des étapes passées
      // + titre actuel ≠ + suivant)
      $filariane.empty()
      for (i = 0; i < currentIndex; i++) {
        $filariane.append(`Étape ${i + 1} : ${etapes.titres[i]} >> `)
      }
      // ajout titre courant
      $filariane.append(`Étape ${currentIndex + 1} : `).append(getElement('span', { class: 'highlight' }, titre))
      // le lien suivant éventuel
      if (etapes.hasNext()) {
        const lien = getLienSuivant()
        $filariane.append(lien)
      }
    } // showEtape

    // les variables communes à nos fcts, affectées dans le try
    let consigne,
      divConsigne,
      divReponse,
      etapes,
      hasConsigne,
      hasReponse,
      hasTexConsigne,
      iframe,
      information,
      isResultatSent,
      reponse,
      sendResultat,
      $filariane,
      $consigne,
      $reponse,
      $information,
      $lienConsigne,
      $lienInfo,
      $lienReponse,
      $page
    try {
      log('urlUi avec ', ressource.parametres, options)
      if (!$ || !$.fn) throw Error('jQuery n’est pas chargé')
      if (!$.ui || typeof $.ui.dialog !== 'function') throw Error('Le module dialog de jQuery n’est pas chargé')
      isResultatSent = false
      const params = ressource.parametres
      const { container } = options
      sendResultat = options.sendResultat
      hasConsigne = params.question_option !== 'off'
      hasReponse = params.answer_option !== 'off'

      $page = $(`#${divIframeId}`)
      // on ajoute tous nos div même si tous ne serviront pas, pour simplifier le code
      // les liens
      const entete = options.entete
      $lienReponse = $(addElement(entete, 'div', { id: 'lienReponse' }, 'Réponse'))
      $lienConsigne = $(addElement(entete, 'div', { id: 'lienConsigne' }, 'Consigne'))
      $lienInfo = $(addElement(entete, 'div', { id: 'lienInfo' }, 'Information'))
      $filariane = $(addElement(entete, 'div', { id: 'filariane' }))
      // les dialogs
      $information = $(addElement(container, 'div', { id: 'information', class: 'invisible' }))
      divConsigne = addElement(container, 'div', { id: 'consigne', class: 'invisible' })
      $consigne = $(divConsigne)
      divReponse = addElement(container, 'div', { id: 'reponse', class: 'invisible' })
      $reponse = $(divReponse)
      if (hasConsigne) {
        if (params.consigne) {
          // log(params.consigne, 'devient', wikiFormat(params.consigne))
          $(divConsigne).html(wikiFormat(params.consigne))
        } else {
          log.error(`Pas de consigne alors que question_option vaut ${params.question_option}`)
          $(divConsigne).text('Pas de consigne donnée.')
        }
      }

      // gestion réponse
      // console.log(`url avec hasReponse ${hasReponse} et cb ${sendResultat ? 'oui' : 'non'} `)
      if (hasReponse) {
        // y'a une réponse à faire
        addReponseDialog()
      } else if (sendResultat) {
        // pas de réponse demandée mais qqun attend un résultat, on ajoute le bouton vu
        page.addBoutonVu(() => sendResultat(null, true))
      }

      etapes = {
        currentIndex: 0,
        // chaque elt est une etape : un tableau avec les objets à afficher (parmi consigne, reponse, information, iframe)
        liste: [],
        // les titres de chaque étape
        titres: [],
        hasNext: function () {
          return etapes.currentIndex < (etapes.liste.length - 1)
        },
        next: function () {
          if (etapes.hasNext()) {
            etapes.currentIndex++
            showEtape()
          }
        }
      }

      /*
       * Nos 4 éléments qui peuvent entrer dans une étape : iframe, information, consigne, reponse
       */

      /* La page en iframe, ou div si swf */
      iframe = {
        name: 'iframe',
        activer: function () {
          $page.show()
        },
        desactiver: function () {
          $page.hide()
        }
      }

      /* objet pour la fenêtre modale information */
      information = {
        name: 'information',
        activer: function () {
          $information.dialog('open')
          highLight($lienInfo)
          $lienInfo.show()
        },
        desactiver: function () {
          $information.dialog('close')
          $lienInfo.hide()
        },
        setContent: function (content) {
          $information.text(content)
          if (arguments.length > 1) {
            for (let i = 1; i < arguments.length; i++) {
              $information.append(arguments[i])
            }
          }
        },
        toggle: function () {
          if ($information.dialog('isOpen')) {
            $information.dialog('close')
            unLight($lienInfo)
          } else {
            $information.dialog('open')
            highLight($lienInfo)
          }
        }
      }

      /* objet pour la fenêtre modale consigne */
      consigne = {
        name: 'consigne',
        activer: function () {
          $consigne.dialog('open')
          highLight($lienConsigne)
          $lienConsigne.show()
          if (hasTexConsigne) {
            // @see https://groups.google.com/forum/#!topic/mathjax-users/v6nVeANKihs
            // http://docs.mathjax.org/en/latest/queues.html
            /* global MathJax */
            MathJax.Hub.Queue(['Typeset', MathJax.Hub, 'consigne'])
          }
        },
        desactiver: function () {
          $consigne.dialog('close')
          $lienConsigne.hide()
        },
        toggle: function () {
          if ($consigne.dialog('isOpen')) {
            $consigne.dialog('close')
            unLight($lienConsigne)
          } else {
            $consigne.dialog('open')
            highLight($lienConsigne)
          }
        }
      }

      /* objet pour la fenêtre modale réponde */
      reponse = {
        name: 'reponse',
        activer: function () {
          $reponse.dialog('open')
          highLight($lienReponse)
          $lienReponse.show()
        },
        desactiver: function () {
          $reponse.dialog('close')
          $lienReponse.hide()
        },
        toggle: function () {
          if ($reponse.dialog('isOpen')) {
            $reponse.dialog('close')
            unLight($lienReponse)
          } else {
            $reponse.dialog('open')
            highLight($lienReponse)
          }
        }
      }

      // ce truc renvoie toujours 0 !!! (il ne compte que le visible ?)
      // const hasTexConsigne = ($consigne.filter('.math-tex').length > 0)
      hasTexConsigne = ($consigne.find('.math-tex').length > 0)
      // const hasTexConsigne = $consigne.html().indexOf('class='math-tex'')

      loadDependencies(function () {
        init()
        setEtapes()
        log(`les étapes : ${etapes.liste.map(etape => etape.map(item => item.name).join('+')).join(' > ')}`, etapes.titres)
        showEtape()
        next()
      })
    } catch (error) {
      console.error(error)
      next(new Error('Une erreur est survenue'))
    }
  }) // $(code)
}

export default displayUi
