import PropTypes from 'prop-types'
import React, { Component, Fragment } from 'react'
import { formValues, Field } from 'redux-form'

import { IntegerField, InputField, TextareaField } from 'client-react/components/fields'
import ShowError from 'client-react/components/ShowError'

const avertMessage = 'Impossible de charger une page http dans une page https, elle sera ouverte dans un autre onglet (donc consigne et réponse ne pourront pas être superposé à son contenu)'

const questionOptions = [
  { key: 'off', label: 'Aucune' },
  { key: 'before', label: 'Avant' },
  { key: 'while', label: 'Pendant' },
  { key: 'after', label: 'Après' }
]
const answerOptions = [
  { key: 'off', label: 'Aucune' },
  { key: 'question', label: 'Avec la consigne' },
  { key: 'while', label: 'Pendant' },
  { key: 'after', label: 'Après' }
]

class EditorUrl extends Component {
  constructor (props) {
    super(props)
    // on teste l'objet window car ce composant pourrait être utilisé pour du rendu coté serveur
    this.isOnHttps = typeof window !== 'undefined' && window.location.protocol === 'https:'
  }

  getHttpsAvert () {
    const { url } = this.props
    if (url) {
      const isHttpsUrl = (url.indexOf('https://') === 0)
      if (this.isOnHttps && !isHttpsUrl) return Error(avertMessage)
    }
    return null
  }

  hasConsigne () {
    return this.props.questionOption !== 'off'
  }

  render () {
    const httpsError = this.getHttpsAvert()
    return (
      <fieldset>
        <div className="grid-3">
          <InputField
            label="Url de la page externe"
            name="parametres[adresse]"
            type="url"
          />
        </div>
        <ShowError error={httpsError} />
        <div className="grid-3">
          <IntegerField
            label="Largeur"
            info="(en pixel, laisser vide pour s’adapter à l’écran)"
            name="parametres[largeur]"
          />
          <IntegerField
            label="Hauteur"
            info="(en pixel, laisser vide pour s’adapter à l’écran)"
            name="parametres[hauteur]"
          />
        </div>
        <div>Consigne
          {questionOptions.map(({ key, label }) => (
            <div className="field" key={key}>
              <label>
                <Field
                  name="parametres[question_option]"
                  className="radio"
                  component="input"
                  type="radio"
                  value={key}
                /> {label} {key !== 'off' && (<i>(l’affichage de la page)</i>)}
              </label>
            </div>
          ))}
          {this.hasConsigne()
            ? (
            <Fragment>
              <p className="info">Vous pouvez utiliser la syntaxe suivante pour la mise en forme :<br/>
                {/* cf https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-comment-textnodes.md#legitimate-uses */}
                {'//texte en italique//'} ⇒ <em>texte en italique</em><br/>
                **texte en gras** ⇒ <strong>texte en gras</strong><br/>
                __texte souligné__ ⇒ <u>texte souligné</u><br/>
                texte^exposant ⇒ texte<sup>exposant</sup><br/>
                <br/>
                Les tags html b, br, em, i, strong, sub, sup, u sont également autorisés (tous les autres seront affichés sans être interprétés), par exemple :<br/>
                H&lt;sub&gt;2&lt;/sub&gt;O ⇒ H<sub>2</sub>O
              </p>
              <TextareaField
                label="Texte de la consigne"
                name="parametres[consigne]"
                cols="80"
                rows="20"
              />
            </Fragment>
              )
            : null }
        </div>
        <div>Réponse
          {answerOptions.map(({ key, label }) => (
            <div className="field" key={key}>
              <label>
                <Field
                  name="parametres[answer_option]"
                  className="radio"
                  component="input"
                  type="radio"
                  value={key}
                /> {label}
              </label>
            </div>
          ))}
        </div>
      </fieldset>
    )
  }
}

EditorUrl.propTypes = {
  url: PropTypes.string,
  questionOption: PropTypes.string
}

const propsFromFormValues = {
  questionOption: 'parametres[question_option]',
  url: 'parametres[adresse]'
}

export default formValues(propsFromFormValues)(EditorUrl)
