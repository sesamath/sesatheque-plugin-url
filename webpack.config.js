const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = () => ({
  entries: {},
  plugins: [
    new CopyWebpackPlugin({
      patterns: [{
        from: path.resolve(__dirname, 'public', 'url.gif'),
        to: 'plugins/url/[name][ext]'
      }]
    })
  ],
  rules: []
})
