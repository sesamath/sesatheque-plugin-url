import './public/url.css'
import dom from 'sesajstools/dom'
import loadSwf from 'sesajstools/dom/loadSwf'
import log from 'sesajstools/utils/log'

import page from 'client/page'

// attention à mettre la même chose que dans displayUi et le css
const divIframeId = 'page'

/**
 * Affiche les ressources de type url (page externe) en créant une iframe dans le container (ou un div si l'url est un swf)
 * @service plugins/url/display
 * @param {Ressource}      ressource  L'objet ressource
 * @param {displayOptions} options    Les options après init
 * @param {errorCallback}  next       La fct à appeler quand le contenu sera chargé
 */
export const display = (ressource, options, next) => {
  /**
   * Ajoute le bouton vu et le listener sur unload quand il n'y a pas de réponse demandée
   */
  function addDefaultResultatCb () {
    // un listener pour envoyer 'affiché' comme score (i.e. un score de 1 avec une durée)
    options.container.addEventListener('unload', function () {
      if (isLoaded && !isResultatSent) sendResultat(null, true)
    })
    // le bouton vu
    page.addBoutonVu(function () {
      sendResultat(null, true)
    })
  }

  /**
   * Ajoute l'iframe (ou un div si c'est un swf directement)
   * @private
   */
  function addPage (url, params, next) {
    log('addPage avec les params', params)
    const divIframe = dom.addElement(options.container, 'div', { id: divIframeId, class: 'invisible' })
    const divIframeSrcId = 'urlSrc'
    const source = dom.addElement(divIframe, 'p', { id: divIframeSrcId }, 'source : ')
    dom.addElement(source, 'a', { href: url, target: '_blank', rel: 'noopener noreferer' }, url)
    dom.addText(source, ' (cliquez sur le lien pour ouvrir le contenu dans un nouvel onglet si rien ne s’affiche ci-dessous)')
    const autosizeBlocs = [divIframeSrcId]
    const autosizeOptions = { offsetHeight: 0, offsetWidth: 40 }
    // toujours autosize sur le conteneur, sauf pour les swf
    const isSwf = /^[^?]+\.swf(\?.*)?$/.test(url)
    if (!isSwf) {
      page.autosize(divIframeId, autosizeBlocs, null, autosizeOptions)
    }
    // url sera ajouté après l'appel de afterLoading, pour éviter que l'eventListener soit ajouté après le load (si c'est en cache)
    const args = { id: 'pageContent' }

    // test proto compatible
    if (window.location.protocol === 'https:' && !/^https:/.test(url)) {
      const p = dom.addElement(divIframe, 'p', {}, 'La page externe demandée ne peut être incorporée ici car elle n’est pas en https. Vous pouvez ')
      dom.addElement(p, 'a', { href: url, target: '_blank' }, 'l’ouvrir dans un nouvel onglet')
      dom.addText(p, '.')

      // l'iframe, mais on fait un cas particulier pour les urls en swf qui ne renvoient pas un DOMDocument
      // (car ff aime pas et sort une erreur js Permission denied to access property 'toString'
      // et chrome râle aussi parce que c'est pas un document)
    } else if (isSwf) { // faut pas prendre les truc.php?toto=truc.swf
      // par ex https://bibliotheque.sesamath.local/public/voir/5946
      const swfOptions = {
        autosize: true,
        width: params.largeur ?? 800,
        height: params.hauteur ?? 600
      }
      log('C’est un swf, on ajoute un div et pas une iframe avec les options', swfOptions)
      loadSwf(divIframe, url, swfOptions, next)
      return // car next sera appelé en async

      // 2e cas particulier pour une image, pas besoin d'iframe pour ça
    } else if (/^[^?]+\.(png|jpe?g|gif)(\?.*)?$/.test(url)) {
      // c'est un tag img
      if (params.hauteur > 100) args.height = params.hauteur
      if (params.largeur > 100) args.width = params.largeur
      log('c’est une image, pas d’iframe mais un tag img', args, params)
      const img = dom.addElement(divIframe, 'img', args)
      img.src = url

      // cas "normal", on peut mettre une iframe vers une page
    } else {
      args.src = url
      dom.addElement(divIframe, 'iframe', args, 'Si vous lisez ce texte, votre navigateur ne supporte probablement pas les iframes et ne pourra afficher la page')
      const iframeAutosizeOptions = {
        ...autosizeOptions,
        minHeight: 300,
        minWidth: 300,
        offsetHeight: 80,
        offsetWidth: 60
      }
      page.autosize(args.id, autosizeBlocs, [], iframeAutosizeOptions)
    }
    next()
  } // addPage

  /**
   * Envoie le résultat à resultatCallback
   * @param {string}   reponse
   * @param {boolean}  deferSync
   * @param {function} next
   */
  function sendResultat (reponse, deferSync, next) {
    const resultat = {
      score: 1
    }
    if (deferSync) {
      resultat.fin = true
      resultat.deferSync = true
    }
    if (reponse) resultat.reponse = reponse
    isResultatSent = true
    resultatCallback(resultat, next)
  }

  const { resultatCallback } = options
  let isLoaded
  // pour éviter de reposter au unload si on a cliqué sur le bouton vu avant
  let isResultatSent = false

  try {
    if (!next) {
      next = function displayUiError (error) {
        if (!error) return
        console.error(error)
        page.addError(error)
      }
    }
    // les params minimaux
    if (!ressource) throw new Error('Ressource manquante')
    for (const p of ['oid', 'titre', 'parametres']) {
      if (!ressource[p]) throw new Error(`Ressource incomplète (propriété ${p} manquante)`)
    }
    for (const param of ['base', 'pluginBase', 'container', 'errorsContainer']) {
      if (!options[param]) throw new Error('Paramètre ' + param + ' manquant')
    }
    // raccourcis
    const params = ressource.parametres
    const url = params.adresse
    if (!url) throw new Error('Url manquante')
    params.question_option = params.question_option || 'off'
    params.answer_option = params.answer_option || 'off'
    if (!/^https?:\/\//.test(url)) throw new Error('Url invalide : ' + url)

    // init
    const hasConsigne = params.question_option !== 'off'
    const hasReponse = params.answer_option !== 'off'
    const isBasic = !hasConsigne && !hasReponse
    // ni question ni réponse, pas grand chose à faire
    if (isBasic) {
      return addPage(url, params, () => {
        if (resultatCallback) addDefaultResultatCb()
        const elt = document.getElementById(divIframeId)
        if (elt) elt.classList.remove('invisible')
        isLoaded = true
        next()
      })
    }
    // sinon, faut charger la gestion des dialog jQueryUi
    import('./display.ui').then(({ default: displayUi }) => {
      // on ajoute l'entete avant la page
      const entete = dom.addElement(options.container, 'div', { id: 'entete' })
      addPage(url, params, () => {
        // on ajoute ça pour displayUi
        if (resultatCallback) options.sendResultat = sendResultat
        options.entete = entete
        displayUi(ressource, options, next)
      })
    })
  } catch (error) {
    next(error)
  }
}
