import editor from './EditorUrl'
import type from './type'
import validate from './validate'

const defaultValue = {
  parametres: {
    question_option: 'off',
    answer_option: 'off'
  }
}

export { editor, type, defaultValue, validate }
